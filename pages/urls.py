from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home_view, name='home'),

    # path('', views.HomePageView.as_view(), name='home'),

    #toate produsele
    path('carti/', views.CarteListView.as_view(), name='carte'),
    path('produse/', views.ProdusePageView.as_view(), name='produse'),
    path('promotii/', views.PromotiiPageView.as_view(), name='promotii'),
    path('adauga_carte/', views.CarteCreateView.as_view(), name='adauga_carte'),
    path('detalii_carte/<int:id>/', views.CarteDetailsView.as_view(), name='detalii_carte'),
    path('editeaza_carte/<int:id>/', views.CarteUpdateView.as_view(), name='editeaza_carte'),
    path('sterge_carte/<int:id>/', views.CarteDeleteView.as_view(), name='sterge_carte'),

    path('detalii_carte/<int:pk>/comment/', views.add_comment_to_post, name='add_comment_to_post'),
    # path('detalii_promotie/<int:pk>/comment/', views.add_comment_to_promotie, name='add_comment_to_promotie'),
    path('comment/<int:pk>/approve/', views.comment_approve, name='comment_approve'),
    path('comment/<int:pk>/remove/', views.comment_remove, name='comment_remove'),

    #categorii
    path('categorie/', views.CategorieListView.as_view(), name='categorie'),
    path('adauga_categorie/', views.CategorieCreateView.as_view(), name='adauga_categorie'),
    path('detalii_categorie/<int:id>/', views.CategorieDetailView.as_view(), name='detalii_categorie'),
    path('editeaza_categorie/<int:id>/', views.CategorieUpdateView.as_view(), name='editeaza_categorie'),
    path('sterge_categorie/<int:id>/', views.CategorieDeleteView.as_view(), name='sterge_categorie'),

    #edituri
    path('edituri/', views.EdituraListView.as_view(), name='editura'),
    path('adauga_editura/', views.EdituraCreateView.as_view(), name='adauga_editura'),
    path('detalii_editura/<int:id>/', views.EdituraDetailView.as_view(), name='detalii_editura'),
    path('editeaza_editura/<int:id>/', views.EdituraUpdateView.as_view(), name='editeaza_editura'),
    path('sterge_editura/<int:id>/', views.EdituraUpdateView.as_view(), name='sterge_editura'),

    #promotii
    path('promotie/', views.PromotieListView.as_view(), name='promotie'),
    path('adauga_promotie/', views.PromotieCreateView.as_view(), name='adauga_promotie'),
    path('detalii_promotie/<int:id>/', views.PromotieDetailView.as_view(), name='detalii_promotie'),
    path('editeaza_promotie/<int:id>/', views.PromotieUpdateView.as_view(), name='editeaza_promotie'),
    path('sterge_promotie/<int:id>/', views.PromotieDeleteView.as_view(), name='sterge_promotie'),

    #pagina principala
    path('', views.home_view, name='home'),

    #categorii + edituri
    path('fictiune/', views.CategorieFictiuneView.as_view(), name='fictiune'),
    path('stiinte_umaniste/', views.CategorieStiinteUmanisteView.as_view(), name='stiinte_umaniste'),
    path('editura_rao/', views.EdituraRaoView.as_view(), name='editura_rao'),
    path('editura_humanitas/', views.EdituraHumanitasView.as_view(), name='editura_humanitas'),
    path('editura_nemira/', views.EdituraNemiraView.as_view(), name='editura_nemira'),
    path('editura_paladin/', views.EdituraPaladinView.as_view(), name='editura_paladin'),

    #Favorite
    path('favorite/adauga_la_favorite/<int:id>/', views.adauga_la_favorite, name='favorite_prod'),
    path('favorite/', views.favorite_lista, name='favorite'),

    #Error 404
    path('error404/', views.error404, name='error404')

]