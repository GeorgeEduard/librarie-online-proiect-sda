from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from .forms import CommentForm, SignUpForm, LoginForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.urls import reverse_lazy

from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView


from .models import Carte, Categorie, Promotie, Editura, Comentariu

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


def home_view(request):
    return render(request, 'home.html')

def error404(request):
    return render(request, '404.html')

def signup_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

def login_view(request):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('home')
    else:
        form = LoginForm()
    return render(request, 'registration/login.html', {'form': form})


# def home_page_view(request):
#     return HttpResponse('carte')


# class HomePageView(TemplateView):
#     template_name = 'base_admin.html'
#     context_object_name = 'carte'


class ProdusePageView(ListView):
    model = Carte
    template_name = 'produse.html'
    context_object_name = 'produs'


class PromotiiPageView(ListView):
    model = Promotie
    template_name = 'promotii.html'
    context_object_name = 'promotie'


def add_comment_to_post(request, pk):
    post = get_object_or_404(Carte, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('detalii_carte', id=post.pk)
    else:
        form = CommentForm()
    return render(request, 'add_comment_to_post.html', {'form': form})

@login_required
def comment_approve(request, pk):
    comment = get_object_or_404(Comentariu, pk=pk)
    comment.approve()
    return redirect('detalii_carte', id=comment.post.pk)

@login_required
def comment_remove(request, pk):
    comment = get_object_or_404(Comentariu, pk=pk)
    comment.delete()
    return redirect('detalii_carte', id=comment.post.pk)


# def add_comment_to_promotie(request, pk):
#     post = get_object_or_404(Promotie, pk=pk)
#     if request.method == "POST":
#         form = CommentForm(request.POST)
#         if form.is_valid():
#             comment = form.save(commit=False)
#             comment.post_promotie = post
#             comment.save()
#             return redirect('detalii_promotie', id=post.pk)
#     else:
#         form = CommentForm()
#     return render(request, 'add_comment_to_promotie.html', {'form': form})
#
# @login_required
# def comment_approve(request, pk):
#     comment = get_object_or_404(Comentariu, pk=pk)
#     comment.approve()
#     return redirect('detalii_promotie', pk=comment.post_promotie.pk)
#
# @login_required
# def comment_remove(request, pk):
#     comment = get_object_or_404(Comentariu, pk=pk)
#     comment.delete()
#     return redirect('detalii_promotie', pk=comment.post_promotie.pk)


@login_required
def adauga_la_favorite(request, id):
    produs = get_object_or_404(Carte, id=id)
    if produs.favorite.filter(id=request.user.id).exists():
        produs.favorite.remove(request.user)
    else:
        produs.favorite.add(request.user)
    return HttpResponseRedirect(request.META["HTTP_REFERER"])

def favorite_lista(request):
    iteme = Carte.objects.filter(favorite=request.user)
    return render(request, 'favorite.html', {"iteme": iteme})


class CarteListView(ListView):
    model = Carte
    template_name = 'carte_list_view.html'
    context_object_name = 'carte'

class CarteDetailsView(DetailView):
    model = Carte
    template_name = 'carte_detail_view.html'
    context_object_name = 'carte'
    pk_url_kwarg = 'id'

class CarteCreateView(LoginRequiredMixin, CreateView):
    model = Carte
    template_name = 'carte_create_view.html'
    fields = '__all__'
    success_url = reverse_lazy('carte')
    context_object_name = 'carte'
    pk_url_kwarg = 'id'

class CarteUpdateView(LoginRequiredMixin, UpdateView):
    model = Carte
    template_name = 'carte_update_view.html'
    fields = '__all__'
    success_url = reverse_lazy('carte')
    context_object_name = 'carte'
    pk_url_kwarg = 'id'

class CarteDeleteView(LoginRequiredMixin, DeleteView):
    model = Carte
    template_name = 'carte_delete_view.html'
    success_url = reverse_lazy('carte')
    context_object_name = 'carte'
    pk_url_kwarg = 'id'


class CategorieListView(ListView):
    model = Categorie
    template_name = 'categorie_list_view.html'
    context_object_name = 'categorie'

class CategorieDetailView(DetailView):
    model = Categorie
    template_name = 'categorie_detail_view.html'
    context_object_name = 'categorie'
    pk_url_kwarg = 'id'

class CategorieCreateView(LoginRequiredMixin, CreateView):
    model = Categorie
    template_name = 'categorie_create_view.html'
    fields = '__all__'
    success_url = reverse_lazy('categorie')

class CategorieUpdateView(LoginRequiredMixin, UpdateView):
    model = Categorie
    template_name = 'categorie_update_view.html'
    fields = '__all__'
    success_url = reverse_lazy('categorie')
    context_object_name = 'categorie'
    pk_url_kwarg = 'id'

class CategorieDeleteView(LoginRequiredMixin, DeleteView):
    model = Categorie
    template_name = 'categorie_delete_view.html'
    success_url = reverse_lazy('categorie')
    context_object_name = 'categorie'
    pk_url_kwarg = 'id'


class EdituraListView(ListView):
    model = Editura
    template_name = 'editura_list_view.html'
    context_object_name = 'editura'

class EdituraDetailView(DetailView):
    model = Editura
    template_name = 'editura_detail_view.html'
    context_object_name = 'editura'
    pk_url_kwarg = 'id'

class EdituraCreateView(LoginRequiredMixin, CreateView):
    model = Editura
    template_name = 'editura_create_view.html'
    fields = '__all__'
    success_url = reverse_lazy('editura')
    context_object_name = 'editura'
    pk_url_kwarg = 'id'

class EdituraUpdateView(LoginRequiredMixin, UpdateView):
    model = Editura
    template_name = 'editura_update_view.html'
    fields = '__all__'
    success_url = reverse_lazy('editura')
    context_object_name = 'editura'
    pk_url_kwarg = 'id'

class EdituraDeleteView(LoginRequiredMixin, DeleteView):
    model = Editura
    template_name = 'editura_delete_view.html'
    success_url = reverse_lazy('editura')
    context_object_name = 'editura'
    pk_url_kwarg = 'id'


class PromotieListView(ListView):
    model = Promotie
    template_name = 'promotie_list_view.html'
    context_object_name = 'promotie'

class PromotieDetailView(DetailView):
    model = Promotie
    template_name = 'promotie_detail_view.html'
    context_object_name = 'promotie'
    pk_url_kwarg = 'id'

class PromotieCreateView(LoginRequiredMixin, CreateView):
    model = Promotie
    template_name = 'promotie_create_view.html'
    fields = '__all__'
    success_url = reverse_lazy('promotie')
    context_object_name = 'promotie'
    pk_url_kwarg = 'id'

class PromotieUpdateView(LoginRequiredMixin, UpdateView):
    model = Promotie
    template_name = 'promotie_update_view.html'
    fields = '__all__'
    success_url = reverse_lazy('promotie')
    context_object_name = 'promotie'
    pk_url_kwarg = 'id'

class PromotieDeleteView(LoginRequiredMixin, DeleteView):
    model = Promotie
    template_name = 'promotie_delete_view.html'
    success_url = reverse_lazy('promotie')
    context_object_name = 'promotie'
    pk_url_kwarg = 'id'


class CategorieFictiuneView(ListView):
    model = Carte
    template_name = 'fictiune.html'
    context_object_name = 'fictiune'

class CategorieStiinteUmanisteView(ListView):
    model = Carte
    template_name = 'stiinte_umaniste.html'
    context_object_name = 'stiinte_umaniste'

class EdituraRaoView(ListView):
    model = Carte
    template_name = 'editura_rao.html'
    context_object_name = 'editura_rao'

class EdituraHumanitasView(ListView):
    model = Carte
    template_name = 'editura_humanitas.html'
    context_object_name = 'editura_humanitas'

class EdituraNemiraView(ListView):
    model = Carte
    template_name = 'editura_nemira.html'
    context_object_name = 'editura_nemira'

class EdituraPaladinView(ListView):
    model = Carte
    template_name = 'editura_paladin.html'
    context_object_name = 'editura_paladin'
