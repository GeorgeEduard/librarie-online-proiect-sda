from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User

from .models import Comentariu
from django import forms

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comentariu
        fields = ('autor', 'continut',)


class SignUpForm(UserCreationForm):
    username = forms.CharField(label=("Nume de utilizator"), max_length=30, required=False, help_text='Introdu numele de utilizator')
    email = forms.EmailField(max_length=254, help_text='Introdu o adresa de email valida')
    password1 = forms.CharField(label=("Parola"), strip=False, widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}))
    password2 = forms.CharField(label=("Confirma parola"), widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
                                strip=False)
    first_name = forms.CharField(label=("Prenume"), max_length=30, required=False, help_text='Optional')
    last_name = forms.CharField(label=("Nume"), max_length=30, required=False, help_text='Optional.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )


class LoginForm(AuthenticationForm):
    username = forms.CharField(label=("Nume de utilizator"), help_text='Introdu numele de utilizator')
    password = forms.CharField(label=("Parola"), strip=False, widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
                               help_text='Introdu parola')



