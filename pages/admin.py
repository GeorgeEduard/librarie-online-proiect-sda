from django.contrib import admin

from django.contrib import admin
from .models import Categorie, Editura, Carte, Promotie, Comentariu

admin.site.register(Categorie)
admin.site.register(Editura)
admin.site.register(Carte)
admin.site.register(Comentariu)
admin.site.register(Promotie)




