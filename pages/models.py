from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone


class Categorie(models.Model):
    nume = models.CharField(max_length=35, blank=True, null=True)

    def __str__(self):
        return self.nume

    class Meta:
        verbose_name = 'Categorie'
        verbose_name_plural = 'Categorii'


class Editura(models.Model):
    nume = models.CharField(max_length=35, blank=True, null=True)

    def __str__(self):
        return self.nume

    class Meta:
        verbose_name = 'Editura'
        verbose_name_plural = 'Edituri'


class Carte(models.Model):
    favorite = models.ManyToManyField(User, related_name="favorite", blank=True)
    imagine = models.ImageField(upload_to='static/img/', blank=True, null=-True)
    titlu = models.CharField(max_length=60, blank=True, null=-True)
    autor = models.CharField(max_length=45, blank=True, null=-True)
    pret = models.FloatField(default=0, blank=True, null=True)
    descriere = models.CharField(max_length=2500)
    editura = models.ForeignKey(Editura, on_delete=models.DO_NOTHING, blank=True, null=True)
    categorie = models.ForeignKey(Categorie, on_delete=models.DO_NOTHING, blank=True, null=True)
    anul_aparitiei = models.IntegerField(default=0, blank=True, null=True)
    nr_pagini = models.IntegerField(default=0, blank=True, null=True)
    cantitate = models.PositiveIntegerField(default=0, blank=True, null=True)
    disponibilitate = models.BooleanField(default=False)

    def __str__(self):
        return self.titlu

    @property
    def imagine_url(self):
        if self.imagine and hasattr(self.imagine, 'url'):
            return self.imagine.url

    class Meta:
        verbose_name = 'Carte'
        verbose_name_plural = 'Carti'

    def get_absolute_url(self):
        return reverse('detalii_carte', kwargs={"id": self.id})


class Promotie(models.Model):
    imagine = models.ImageField(upload_to='static/img/', blank=True, null=-True)
    nume = models.CharField(max_length=35, blank=True, null=True)
    pret_initial = models.FloatField(default=0, blank=True, null=True)
    pret_redus = models.FloatField(default=0, blank=True, null=True)
    reducere = models.CharField(max_length=15, blank=True, null=True)
    disponibilitate = models.BooleanField(default=False)
    descriere = models.CharField(max_length=2500)
    cantitate = models.PositiveIntegerField(default=0, blank=True, null=True)
    categorie = models.ForeignKey(Categorie, on_delete=models.DO_NOTHING, blank=True, null=True)


    def __str__(self):
        return self.nume

    @property
    def imagine_url(self):
        if self.imagine and hasattr(self.imagine, 'url'):
            return self.imagine.url

    class Meta:
        verbose_name = 'Promotie'
        verbose_name_plural = 'Promotii'


class Comentariu(models.Model):
    post = models.ForeignKey('Carte', on_delete=models.CASCADE, related_name='comentarii', blank=True, null=True)
    post_promotie = models.ForeignKey('Promotie', on_delete=models.CASCADE, related_name='comentarii', blank=True, null=True)
    autor = models.CharField(max_length=55, blank=True, null=True)
    email = models.EmailField(max_length=35, blank=True, null=True)
    continut = models.TextField(max_length=250, blank=True, null=True)
    data_creare = models.DateTimeField(default=timezone.now)
    aprobare_comentariu = models.BooleanField(default=False)

    def approve(self):
        self.aprobare_comentariu = True
        self.save()

    def __str__(self):
        return self.continut

    class Meta:
        ordering = ['data_creare']
        verbose_name = 'Comentariu'
        verbose_name_plural = 'Comentarii'

def aprobare_comentariu(self):
    return self.comments.filter(approved_comment=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=150)
    signup_confirmation = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username