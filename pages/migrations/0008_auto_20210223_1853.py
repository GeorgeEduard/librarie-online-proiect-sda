# Generated by Django 3.1.5 on 2021-02-23 16:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0007_remove_comentariu_nume'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comentariu',
            options={'ordering': ['data_creare']},
        ),
        migrations.RemoveField(
            model_name='comentariu',
            name='carte',
        ),
        migrations.RemoveField(
            model_name='comentariu',
            name='utilizator',
        ),
        migrations.AddField(
            model_name='comentariu',
            name='activ',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='comentariu',
            name='email',
            field=models.EmailField(blank=True, max_length=35, null=True),
        ),
        migrations.AddField(
            model_name='comentariu',
            name='nume',
            field=models.CharField(blank=True, max_length=55, null=True),
        ),
        migrations.AddField(
            model_name='comentariu',
            name='postare',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='comentariu', to='pages.carte'),
        ),
        migrations.AlterField(
            model_name='comentariu',
            name='continut',
            field=models.TextField(blank=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='comentariu',
            name='data_creare',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='comentariu',
            name='editat',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
