# Librarie online - Proiect SDA

DESCRIERE SITE:

Eu am facut un site pentru o librarie care vinde carti online. Ca sa nu folosesc template-ul de la django pentru a 
crea/edita/sterge produse, am creat pagini separate pentru aceste actiuni care, in momentul in care se logheaza un user 
cu permisiuni de administrator sa ii apara si acele optiuni de a crea/edita/modifica anumite produse, iar daca se 
logheaza un user normal, nu va vedea acele optiuni. 

O sa aiba doar catva pagini: lista carti, un filtru dupa categorie, editura ..., o pagina de promotii si o sa incerc 
sa adaug si un cos de cumparaturi cu toate paginile necesare lui si o pagina de creare cont nou.


CE FAC URL-URILE:

Am adaugat majoritatea url-urilor pentru creare/editare/stergere. Urmeaza sa creez paginile pentru fiecare, apoi o sa 
adaug url-uri si creez pagini pentru cos, si cele vazute de un user normal. Lista cu produse, promotii ...


Alte detalii:

- Home page: va fi pagina de start. Site-ul o sa se numeasca "TweetWords". Pe pagina de start se va afisa: 
  * pe primele 2 randuri, 3-4 carti
  * pe al 3 lea rand, 3-4 promotii
  
- Carti page: va contine toate cartile afisate in 3-4 carti pe rand. In partea stanga a paginii va fi un filtru unde 
se poate face o filtrare a cartilor.
  
- Promotii page: va contine toate promotiile, iar restul va fi ca in 'Carti'.

- Detalii page: aceasta pagina se va accesa atunci cand vrem sa aflam mai multe detalii despre o carte/promotie anume.
Va contine toate detaliile despre acea carte/promotie.
  
- Cos de cumparaturi page: aici va fi cosul de cumparaturi. Va afisa toate produsele din cos cat si detalii despre 
  plata/transport.
  
- Account page: aici va fi formularul de creare cont cu paginile sale aferente.


Modificari la partea de comentarii:

- am reusit sa fac sa apara comentariile in pagina de detalii a produsului si am reusit si sa adaug un buton de 
  'adaugare comentarii';
  
- o sa am nevoie un pic de ajutor la partea cu 'aprobare comentarii' cat si nevoie de un pic de explicatii pentru o 
eroare pe care o primesc atunci cand dau submit la un nou comentariu


!!!
Am incercat sa adaug cosul de cumparaturi cu functionalitatea sa, dar nu am reusit, am incercat diferite tutoriale ... 
Am sperat ca in weekend o sa am timp sa ma ocup de proiect dar au intervenit diferite probleme si nu am reusit sa fac 
nimic :((.

Astazi o sa creez cateva pagini ce imi trebuiesc si sper ca maine sa reusesc cu cosul de cumnparaturi.

In diferitele tutoriale pe care le-am urmarit am observat ca este ok ca sa "imprumuti" template-ul de pe alte site-uri,
ceea ce am facut si eu. Sper ca nu este o problema. Iti voi spune mai multe miercuri si iti voi arata si cam cum ar 
arata proiectul final.

Multumesc!

# Online bookstore - SDA project

SITE DESCRIPTION:

I made a website for a bookstore that sells books online. So that I don't use the django template to
create/edit/delete products, we have created separate pages for these actions which, when a user logs in
with administrator permissions to defend those options to create/edit/modify certain products, and if
a normal user logs in, he will not see those options.

It will only have a few pages: list of books, a filter by category, publisher..., a promotion page and I will try
to add a shopping basket with all the necessary pages and a new account creation page.


WHAT URLS DO:

I have added most of the urls for creating/editing/deleting. I am going to create the pages for each one, then I will
I add urls and create pages for the basket, and those seen by a normal user. List of products, promotions...


Other details:

- Home page: will be the start page. The site will be called "TweetWords". The home page will display:
  * on the first 2 rows, 3-4 cards
  * for the 3rd time, 3-4 promotions
  
- Book page: it will contain all the books displayed in 3-4 books in a row. On the left side of the page there will be a where filter
you can filter the books.
  
- Promotions page: it will contain all the promotions, and the rest will be like in 'Books'.

- Page details: this page will be accessed when we want to find out more details about a specific book/promotion.
It will contain all the details about that book/promotion.
  
- Shopping cart page: here will be the shopping cart. It will show all the products in the basket as well as details about them
  payment/transportation.
  
- Account page: here will be the account creation form with its related pages.


Changes to the comments section:

- I managed to make the comments appear on the product details page and I also managed to add a button
  'add comments';
  
- I will need a little help with the 'comment approval' part, as well as a little explanation for a
error I get when I submit a new comment


!!!
I tried to add the shopping cart with its functionality, but I didn't succeed, I tried different tutorials ...
I hoped that in the weekend I would have time to deal with the project, but various problems intervened and I was not able to do it
nothing :((.

Today I will create some pages that I need and I hope that tomorrow I will succeed with the shopping basket.

In the various tutorials that I followed, I noticed that it is ok to "borrow" the template from other sites,
what I did too. I hope it's not a problem. I will tell you more on Wednesday and show you how it would be
show the final project.

Thanks!
